"""
MicroPyServer is a simple HTTP server for MicroPython projects.

@see https://github.com/troublegum/micropyserver

The MIT License

Copyright (c) 2019 troublegum. https://github.com/troublegum/micropyserver

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import re
import socket
import sys
import io


class Request():
    INVALID = ":("

    def __init__(self, request):
        self.request_raw = request
        self._path = None
        self._method = None
    
    def is_empty(self):
        return len(self.request_raw) == 0
    
    def path(self):
        if self._path is None:
            self._path = re.search("^[A-Z]+\\s+([a-zA-Z0-9\\.\\&\\/\\?\\:@\\-_=#]+)\\s", self.request_raw).group(1)
            if self._path is None:
                self._path = Request.INVALID
        return self._path
    
    def method(self):
        if self._method is None:
            self._method = re.search("^([A-Z]+)", self.request_raw).group(1)
            if self._method is None:
                self._method = Request.INVALID
        return self._method
    
    def is_valid(self):
        return not self.is_empty() and self.path != Request.INVALID and self.method() != Request.INVALID


class MicroPyServer:
    def __init__(self, host="0.0.0.0", port=80, timeout=10):
        """ Constructor """
        self._host = host
        self._port = port
        self._timeout = timeout

        self._routes = []

        self._on_not_found_handler = lambda server, _: server.send("Not found", "404 Not Found")
        self._on_error_handler = lambda err: self._default_on_error_handler(err)

        self._connect = None
        self._sock = None

    def start(self):
        """ Start server """
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._sock.settimeout(self._timeout)
        self._sock.bind((self._host, self._port))
        self._sock.listen(1)
        
        print("Server start")
        while True:
            if self._sock is None:
                break
            try:
                self._connect, _ = self._sock.accept()
                request = self.get_request(self._connect)
                if request.is_empty():
                    self._connect.close()
                    continue
                
                self.find_route_handler(request)(self, request)
                
            except OSError as e:
                # timeout, just retry
                pass
            except Exception as e:
                self._on_error_handler(e)
            finally:
                if self._connect is not None:
                    self._connect.close()

    def stop(self):
        """ Stop the server """
        if self._connect is not None:
            self._connect.close()
        if self._sock is not None:
            self._sock.close()
            self._sock = None

        print("Server stop")

    def add_route(self, path, handler, method="GET"):
        """ Add new route  """
        self._routes.append({"path": path, "handler": handler, "method": method})

    def send_direct(self, data):
        """ Send data to client """
        if self._connect is None:
            raise Exception("Can't send response, no connection instance")
        
        if type(data) is str:
            data = data.encode('utf-8')

        self._connect.sendall(data)

    def send(self, data, status="200 OK", additional_headers={}, content_type="text/plain"):
        """ Send data to client, prepend header"""
        headers = {"Content-Type": content_type, "Server": "MicroPyServer"}
        data_bytes = bytes() if data is None else data.encode()
        headers["Content-Length"] = str(len(data_bytes))
        headers.update(additional_headers)
        
        self.send_direct("HTTP/1.0 {}\r\n".format(status))
        for key, value in headers.items():
            self.send_direct("{}: {}\r\n".format(key, value))
        
        if data is not None:
            self.send_direct("\r\n")
            self.send_direct(data_bytes)

    def find_route_handler(self, request):
        """ Find route """
        for route in self._routes:
            if request.method() == route["method"]:
                if request.path() == route["path"] or re.search("^" + route["path"] + "$", request.path()):
                    return route["handler"]
        
        return self._on_not_found_handler

    def get_request(self, connection, buffer_length=4096):
        """ Return request body """
        return Request(str(connection.recv(buffer_length), "utf8"))

    def on_not_found(self, handler):
        """ Set not found handler """
        self._on_not_found_handler = handler

    def on_error(self, handler):
        """ Set error handler """
        self._on_error_handler = handler

    def _default_on_error_handler(self, error):
        """ Default internal error handler """
        if "print_exception" in dir(sys):
            output = io.StringIO()
            sys.print_exception(error, output)
            str_error = output.getvalue()
            output.close()
        else:
            str_error = str(error)
        
        print(str_error)
        self.send("Error: " + str_error, "500 Internal Server Error")
        