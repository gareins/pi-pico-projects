# pyright: reportWildcardImportFromLibrary=false

from utime import *

def monotonic():
    return ticks_ms() / 1000

def monotonic_ns():
    return ticks_us() * 1000