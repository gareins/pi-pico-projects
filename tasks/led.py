from machine import Pin
import pyRTOS
import time

def led_task(self):
    ### Setup code here
    sleep_time = self.recv()[0]
    time_0 = time.ticks_us()
    counter = 0
    led = Pin("LED", Pin.OUT)
    ### End Setup code

    # Pass control back to RTOS
    yield

    # Main Task Loop
    while True:
        ### Work code here
        led.toggle()

        elapsed = (time.ticks_us() - time_0) / 1000_000
        if counter > 0:
            avg = elapsed / counter
            print(elapsed, avg)
        counter += 1
        ### End Work code

        yield [pyRTOS.timeout(sleep_time)]