from utils.i2c_slave import I2CSlave
import pyRTOS
import constants

def i2c_slave_task(self):
    ### Setup code here
    i2c_bus = I2CSlave(0, sda=0, scl=1, slaveAddress=constants.I2C_SLAVE_ADDRESS)
    switch_data = []
    ### End Setup code

    # Pass control back to RTOS
    yield
    
    # Main Task Loop
    while True:
        # get all new switch action
        switch_data.extend([msg.message for msg in self.recv() if msg.type == constants.LIGHT_TOGGLE_MESSAGE])
        
        """
        # detect stop, sanity check
        if i2c_bus.stopDetect():
            if len(switch_data) > 0:
                 print("Error: should not be stopping while data still needs to be written")
        """
                 
        # detect read request, sanity check
        if i2c_bus.any():
            print("Error: this device does not support master write operations, only reading is available")
            i2c_bus.get() # clear the buffer

        # detect write request, send all switch data and clear, 
        elif i2c_bus.anyRead():
            for _ in range(constants.I2C_MESSAGE_LENGTH):
                if len(switch_data) > 0:
                    i2c_bus.put((switch_data.pop()) & 0xff)
                else:
                    i2c_bus.put(0)
        
        # if no action on i2c, yield
        else:
            yield [pyRTOS.timeout(0.1)] # yield after sending all data and receiving stop
        