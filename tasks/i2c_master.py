import machine
import constants
import pyRTOS

def i2c_master_task(self):
    ### Setup code here
    i2c = machine.I2C(0, scl=machine.Pin(17), sda=machine.Pin(16))
    receiver_task = self.recv()[0]
    ### End Setup code

    # Pass control back to RTOS, wait at least 1 second so that the slave is online first
    yield [pyRTOS.timeout(1)]
    
    # Main Task Loop
    while True:
        try:
            i2c_data = i2c.readfrom(constants.I2C_SLAVE_ADDRESS, constants.I2C_MESSAGE_LENGTH)
        except OSError:
            print("I2C not connected, TODO remove")
            yield [pyRTOS.timeout(20)]
            continue
            
        for light in i2c_data:
            if light > 0:
                self.send(pyRTOS.Message(constants.LIGHT_TOGGLE_MESSAGE,
				                         self,
				                         receiver_task,
				                         light))

        yield [pyRTOS.timeout(constants.TIMEOUT_I2C_MASTER)]