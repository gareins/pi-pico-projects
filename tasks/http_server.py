from utils.micropyserver import MicroPyServer
import utils.index_html_template as templates
import network
import _thread
import pyRTOS
import constants


class HttpServer(MicroPyServer):
    def __init__(self, light_status, toggle_queue):
        super().__init__()
        self.light_status = light_status
        self.toggle_queue = toggle_queue


def reconnect(wlan):
    wlan.active(False)
    yield [pyRTOS.timeout(1)]

    wlan.active(True)
    wlan.connect(constants.SSID, constants.PASSWORD)
    print("connecting...")

    for _ in range(10):
        if not wlan.isconnected():
            yield [pyRTOS.timeout(1)]
        else:
            print("Connected... IP: " + wlan.ifconfig()[0])
            return


def handler_index(server, request):
    list_items = ""
    for x, light in enumerate(server.light_status):
        list_items += templates.LIST_ITEM_TEMPLATE.format("🌝" if light else "🌑", x)

    server.send(templates.MAIN_TEMPLATE.format(list_items), content_type="text/html")


def handler_toggle(server, request):
    num = int(request.path().split("/")[-1])
    if num < 1 or num > constants.NUMBER_OF_LIGHTS + 1:
        server.send("Lucka ne obstaja: " + str(num), status="404 Not Found")
    else:
        server.toggle_queue.append(num + 1) 
        server.send(None, additional_headers={"Location": "/"}, status="303 See Other")


def start_http_server(light_status, toggle_queue):    
    server = HttpServer(light_status, toggle_queue)

    ''' add route '''
    server.add_route("/", handler_index)
    server.add_route("/toggle/\\d+", handler_toggle)

    ''' start server '''
    _thread.start_new_thread(lambda: server.start(), tuple())

    return server


def http_server_task(self):
    ### Setup code here
    server = None
    [light_status, toggle_queue] = self.recv()

    wlan = network.WLAN(network.STA_IF)
    print("wlan initialized")
    ### End Setup code

    # Pass control back to RTOS
    yield

    # Main Task Loop
    while True:
        if not wlan.isconnected():
            # if not connected, but server runnning, stop server
            if server is not None:
                server.stop()
                server = None
                
            # now reconnect with inner yields
            for y in reconnect(wlan):
                yield y
            
        else:
            # if connected but server not running, start the http server
            if server is None:
                server = start_http_server(light_status, toggle_queue)
            
            # and chill...
            yield [pyRTOS.timeout(15)]