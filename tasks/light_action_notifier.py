from machine import Pin
from constants import LIGHT_TOGGLE_MESSAGE
import pyRTOS

map_inputs_lights = [
    1, 1, 1, 1, 1, # upper six pins
    None, #empty space
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, # rest of this side
    None, None, # the lowest 2 pins are used for i2c
    3, 3, 3, # rest ot the low six pins on the other side
    4, 4, 4, # spaced out input
    # the rest is unused and power connections
]

class InputPin:
    def __init__(self, idx, light):
        self.pin = Pin(idx, mode=Pin.IN, pull=Pin.PULL_DOWN)
        self.light = light
        self.previous_value = None
    
    def value(self):
        return self.pin.value()


def light_action_notifier_task(self):
    ### Setup code here
    pins = [InputPin(idx, light) for idx, light in enumerate(map_inputs_lights) if light is not None]
    receiver_task = self.recv()[0]
    ### End Setup code

    # Pass control back to RTOS
    yield
    
    # Main Task Loop
    while True:
        for pin in pins:
            new_value = pin.value()
            if pin.previous_value == 0 and new_value == 1:
                self.send(pyRTOS.Message(LIGHT_TOGGLE_MESSAGE,
				                         self,
				                         receiver_task,
				                         pin.light))

            pin.previous_value = new_value

        yield [pyRTOS.timeout(0.05)]



    