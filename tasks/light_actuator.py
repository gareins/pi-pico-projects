import constants
import pyRTOS
import machine

MAP_PINS = [1,2,3,4,5,6,7,8,9,10,11]
assert(len(MAP_PINS) == constants.NUMBER_OF_LIGHTS)


def toggle_light(light, light_statuses, light_pins):
    if type(light) is not int or light < 1 or light > constants.NUMBER_OF_LIGHTS:
        print("Bad request for light toggle: ", (light, ))
    else:
        index = light - 1
        light_statuses[index] = not light_statuses[index]
        light_pins[index].toggle()

def light_actuator_task(self):
    ### Setup code here
    [light_status, toggle_queue] = self.recv()
    lights = [machine.Pin(pin, machine.Pin.OUT, machine.Pin.PULL_DOWN) for pin in MAP_PINS]
    ### End Setup code

    # Pass control back to RTOS
    yield
    
    # Main Task Loop
    while True:
        for msg in self.recv():
            if msg.type == constants.LIGHT_TOGGLE_MESSAGE:
                toggle_light(msg.message, light_status, lights)
        
        while len(toggle_queue) > 0:
            toggle_light(toggle_queue.popleft(), light_status, lights)
        
        yield [pyRTOS.timeout(constants.TIMEOUT_LIGHT_ACTUATOR)]