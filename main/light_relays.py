import pyRTOS
import tasks
import constants
from collections import deque

light_status = [False] * constants.NUMBER_OF_LIGHTS
http_light_toggles = deque(tuple(), 8)

led_task = pyRTOS.Task(tasks.led_task, priority=5, name="led_task", mailbox=True)
led_task.deliver(1)

http_server_task = pyRTOS.Task(tasks.http_server_task, priority=3, name="http_task", mailbox=True)
http_server_task.deliver(light_status)
http_server_task.deliver(http_light_toggles)

i2c_master_task = pyRTOS.Task(tasks.i2c_master_task, priority=constants.PRIORITY_I2C_MASTER, name="i2c_master", mailbox=True)
i2c_master_task.deliver(light_status)
i2c_master_task.deliver(http_light_toggles)

light_actuator_task = pyRTOS.Task(tasks.light_actuator_task, priority=4, name="light_actuator", mailbox=True)
light_actuator_task.deliver(light_status)
light_actuator_task.deliver(http_light_toggles)


for task in [led_task, http_server_task, i2c_master_task, light_actuator_task]:
    pyRTOS.add_task(task)
    
pyRTOS.start()