import pyRTOS
import tasks

i2c_task = pyRTOS.Task(tasks.i2c_slave_task, priority=4, name="i2c_task", mailbox=True)
light_action_notifier_task = pyRTOS.Task(tasks.light_action_notifier_task, priority=3, name="light_action_notifier", mailbox=True)
light_action_notifier_task.deliver(i2c_task)

for task in [i2c_task, light_action_notifier_task]:
    pyRTOS.add_task(task)
    
pyRTOS.start()